import React from 'react';
import './App.css';
import ComponentName from './ComponentName';

function App() {
  return (
    <div className="App">
      <ComponentName country="Finland"></ComponentName>
    </div>
  );
}

export default App;
